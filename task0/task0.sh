uniqueIps=( $(tcpdump -qns 0 -X -r input/register.pcap | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" | while read -r line ; do
  echo "$line"
done| sort| uniq| xargs))

printf '%s\n' "${uniqueIps[@]}"

