fileSize=$(stat -f%z  input/libclient_comms.so)
echo "start hexdump of libclient_comms.so of size $fileSize"
hexdump -C input/libclient_comms.so > output/libclient_comms.so.hexdump

fileSize=$(stat -f%z  input/libclient_crypt.so)
echo "start hexdump of libclient_crypt.so of size $fileSize"
hexdump -C input/libclient_crypt.so > output/libclient_crypt.so.hexdump
