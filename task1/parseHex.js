const util = require('util');
const fs = require('fs');

function parseFile(filePath, parseFunc) {
    let readAsync = async () => {
    const readFile = util.promisify(fs.readFile);
        let fileBytes = null;
        try {
            fileBytes = await readFile(filePath);
        } catch (err) {
            console.error(err);
            throw new Error('file not found');
        }
        payloads = parseFunc(fileBytes.toString());
        payloads.forEach(function(payload,index) {
                console.log(payload+'\n length: '+payload.length +'\n');
                if(index == 0) {
                    payload = payload.slice(0,514);
                    console.log(payload+'\n length: '+payload.length +'\n');
                }
                let asciiStr = Buffer.from(payload,'hex').toString('ascii');
                console.log(asciiStr+'\n length: '+asciiStr.length +'\n');
          });
    };
    return new Promise((resolve, reject) => {
        readAsync().then( (parsedObj)=> {
            resolve(parsedObj);
        }).catch((error) =>{
            reject(error);
        });
    });
}
function parseTcpData(fileBytes) {
    hexbuffers = fileBytes
                .replace(/\r?\n?/g,'')
                .split(";");
    return hexbuffers;
}
function parseTcpPacket(fileBytes) {
    hexbuffers = fileBytes
                .replace(/\r?\n?/g,'')
                .replace(/\s/g, '')
                .split(";");
    return hexbuffers.map(x => Buffer.from(x,'hex')
    // drop the first 20 bytes that make up the tcp header
                     .slice(20)
                     .toString());
    
}
parseFile("output/asciiContentFormated.txt", parseTcpData);
//parseFile("output/hexdumpFormated.txt",parseTcpPacket);