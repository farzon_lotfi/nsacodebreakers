import idaapi
from ida_funcs import *

opcodeSet = set([])

callSet = set([])

class set_plugin_t(idaapi.plugin_t):
    flags = idaapi.PLUGIN_UNL
    comment = "Function that gets instruction and operand sets"
    help = "This is help"
    wanted_name = "set generator plugin"
    wanted_hotkey = "Alt-F10"

    def init(self):
        return idaapi.PLUGIN_OK

    def run(self, arg):
        print "Start"
        funcIterator()
    def term(self):
        pass

def PLUGIN_ENTRY():
    return set_plugin_t()

def getWinMainFunction():
    return getFuncByName("_WinMain@16");

def callInstruction(opcode, ea):
    if(opcode == "call"):
        operand = idc.GetOpnd(ea, 0)
        callSet.add(operand)

def getInstructions(block):
    ea = block.startEA
    while ea <= block.endEA:
        opcode = idc.GetMnem(ea)
        opcodeSet.add(opcode);
        ea = idc.NextHead(ea)

def funcIterator():
    for i in range(0,get_func_qty()):
        func = getn_func(i)
        getBasicBlock(func)
    print callSet
    print opcodeSet

def getBasicBlock(func):
    flow = idaapi.FlowChart(func)
    edges = [];
    for block in flow:
        getInstructions(block)

